/*
 * ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2011-2014 ICSI Center for Internet Research (ICIR)
 * All rights reserved.
 *
 * See LICENSE.txt for license and terms of usage. 
 *
 * ***** END LICENSE BLOCK *****
 */

pref("extensions.priv3plus.version", "0");

pref("extensions.priv3plus.status", true);

pref("extensions.priv3plus.highlight", false);

pref("extensions.priv3plus.highlightOptionsList", "{\"main\":true,\"iframe\":true,\"other\":true,\"clicked\":true,\"reloaded\":true,\"excepted\":true}");

pref("extensions.priv3plus.allowedExceptionsList", "{}");

pref("extensions.priv3plus.disabledSitesList", "[]");

pref("extensions.priv3plus.disabledPagesList", "[]");

pref("extensions.priv3plus.firstRun", true);

