/*
 * ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2011-2014 ICSI Center for Internet Research (ICIR)
 * All rights reserved.
 *
 * See LICENSE.txt for license and terms of usage. 
 *
 * ***** END LICENSE BLOCK *****
 */

(function(){

	var RESET_EXTENSION_VERSION = "-1";
	
	var guiHelpers = {
		
		setStatusIcons: function(isEnabled)
		{
			var item = null;
			if (isEnabled)
			{
				item = document.getElementById("priv3-toolbar-button");
				if(item)
				{
					item.setAttribute("image", "chrome://priv3plus/content/icons/on2.png");
				}
			}
			else
			{
				item = document.getElementById("priv3-toolbar-button");
				if(item)
				{
					item.setAttribute("image", "chrome://priv3plus/content/icons/off2.png");
				}
			}
		},
		
		setStatusCheckboxes: function(prefix, site, url)
		{
			var item = null;
			// set status info
			item = document.getElementById("priv3-" + prefix + "disable");
			if (item)
			{
				if (statusHelpers.getCurrentStatus())
				{
					item.setAttribute("checked", "false");
				}
				else
				{
					item.setAttribute("checked", "true");
				}
			}
			
			
			item = document.getElementById("priv3-" + prefix + "highlight");
			if (item)
			{
				if (statusHelpers.getHighlightStatus())
				{
					item.setAttribute("checked", "true");
				}
				else
				{
					item.setAttribute("checked", "false");
				}
			}
			
			
			item = document.getElementById("priv3-" + prefix + "disableonsite");
			if (item)
			{
				if (statusHelpers.getSiteStatus(site))
				{
					item.setAttribute("checked", "true");
				}
				else
				{
					item.setAttribute("checked", "false");
				}
			}

			item = document.getElementById("priv3-" + prefix + "disableonpage");
			if (item)
			{
				if (statusHelpers.getPageStatus(url))
				{
					item.setAttribute("checked", "true");
				}
				else
				{
					item.setAttribute("checked", "false");
				}
			}

		},
	}

	var statusHelpers = {
		
		getCurrentStatus : function() {
			return prefHelpers.getPref("status", "boolean");
		},

		setCurrentStatus : function(val){
			prefHelpers.setPref("status", "boolean", val);
		},

		getHighlightStatus : function()
		{
			return prefHelpers.getPref("highlight", "boolean");
		},
		
		setHighlightStatus : function(val)
		{
			prefHelpers.setPref("highlight", "boolean", val);
		},
		
		getSiteStatus: function(site)
		{
			return exceptionHelpers.isDisabledSite(site);
		},
		
		setSiteStatus: function(site, val)
		{
			if (val)
			{
				exceptionHelpers.addDisabledSite(site);
			}
			else
			{
				exceptionHelpers.removeDisabledSite(site);
			}
		},

		getPageStatus: function(url)
		{
			return exceptionHelpers.isDisabledPage(url);
		},
		
		setPageStatus: function(url, val)
		{
			if (val)
			{
				exceptionHelpers.addDisabledPage(url);
			}
			else
			{
				exceptionHelpers.removeDisabledPage(url);
			}
		},
		
		toggleCurrentStatus : function() {
			if (this.getCurrentStatus())
			{
				guiHelpers.setStatusIcons(false);
				this.setCurrentStatus(false);
			}
			else
			{
				guiHelpers.setStatusIcons(true);
				this.setCurrentStatus(true);
			}
		},
		
		toggleHighlight : function()
		{
			if (this.getHighlightStatus())
			{
				this.setHighlightStatus(false);
			}
			else
			{
				this.setHighlightStatus(true);
			}

			highlightHelpers.toggleHighlightForAllBrowsers();		
		},
		
		toggleSiteStatus: function(site)
		{
			if (this.getSiteStatus(site))
			{
				this.setSiteStatus(site, false);
			}
			else
			{
				this.setSiteStatus(site, true);
			}
		},
		
		togglePageStatus: function(url)
		{
			if (this.getPageStatus(url))
			{
				this.setPageStatus(url, false);
			}
			else
			{
				this.setPageStatus(url, true);
			}
		},
		
	}

	var loadHelpers = {
	
		loadCurrentStatus : function()
		{
			if (statusHelpers.getCurrentStatus())
			{
				guiHelpers.setStatusIcons(true);
			}
			else
			{
				guiHelpers.setStatusIcons(false);
			}
		},
	}

	var menuHelpers = {

		populateStatsMenu : function(filteredItems, exceptedItems, reloadedItems)
		{
			var filtered = {};

			if (filteredItems)
			{
				filteredItems.sort(function(a, b)
					{
						return a[0].localeCompare(b[0]);
					});
	
				for(var i = 0; i < filteredItems.length; i++)
				{
					var plugin = filteredItems[i][0];
					if (!reloadedItems[plugin])
					{
						var req = filteredItems[i][1];
						if (filtered[plugin] == null)
						{
							filtered[plugin] = [];
						}
						filtered[plugin].push(req);
					}
				}
			}

			var excepted = {};
			
			if (exceptedItems)
			{
				exceptedItems.sort(function(a, b)
					{
						return a[0].localeCompare(b[0]);
					});

				for (var i = 0; i < exceptedItems.length; i++)
				{
					var plugin = exceptedItems[i][0];
					if (excepted[plugin] == null)
					{
						excepted[plugin] = [];
					}
				}
			}
			
			var reloaded = {};
			
			if (reloadedItems)
			{
				for (var i in reloadedItems)
				{
					var plugin = i;
					if (reloaded[plugin] == null)
					{
						reloaded[plugin] = {};
					}
					for (var t in reloadedItems[i])
					{
						var reqs = reloadedItems[i][t];
						if (reloaded[plugin][t] == null)
						{
							reloaded[plugin][t] = [];
						}

						for (var j = 0; j < reqs.length; j++)
						{
							reloaded[plugin][t].push(req);
						}
					}
				}
			}
			
			return [filtered, excepted, reloaded];
		},
		
		populateOptionsPopup : function(menupopup, type)
		{
			var el = menupopup.firstChild;
			while(el != null) {
				var next = el.nextSibling;
				menupopup.removeChild(el);
				el = next;
			}
			
			var items = ["reportissue", "sep", "disable",
				"sep",
				"disableonsite", "disableonpage",
				"sep", "exceptionlist", 
				"highlight", 
				"highlightoptions",
				"sep", "about"];
			var labels = {};
			labels["reportissue"] = "Report issue on this page";
			labels["disable"] = "Disable Priv3+";
			labels["disableonsite"] = "Disable Priv3+ on this site";
			labels["disableonpage"] = "Disable Priv3+ on this page";
			labels["exceptionlist"] = "Exception options";
			labels["highlight"] = "Highlight third party content";
			labels["highlightoptions"] = "Highlight options";
			labels["about"] = "About Priv3+ (Build: " + Priv3.buildInfo + ")";
			
			for (var i = 0; i < items.length; i++)
			{
				if (items[i] == "sep")
				{
					var sep = document.createElement("menuseparator");
					sep.setAttribute("id", "priv3-" + type + "-options_separator");
					menupopup.appendChild(sep);
				}
				else
				{
					var id = "priv3-" + type + "-" + items[i];
					var menuitem = document.createElement("menuitem");
					menuitem.setAttribute("id", id);
					menuitem.setAttribute("label", labels[items[i]]);
					if (items[i] == "reportissue" || items[i] == "exceptionlist" || items[i] == "about" || items[i] == "highlightoptions")
					{
						// do nothing
					}
					else
					{
						menuitem.setAttribute("type", "checkbox");
					}
	
					if (type == "toolbar")
					{
						menuitem.addEventListener("mouseup", function(event, info) {
							return function(info) {
								Priv3.Priv.onMenuItemCommand(event, this.id.substring(this.id.lastIndexOf("-") + 1), "")
							}
						}(menuitem), false); 
					}
					else if (type == "tools")
					{
						menuitem.addEventListener("command", function(event, info) {
							return function(info) {
								Priv3.Priv.onMenuItemCommand(event, this.id.substring(this.id.lastIndexOf("-") + 1), "")
							}
						}(menuitem), false);
					}
					
					menupopup.appendChild(menuitem);
				}
			}
		},
		
		createToolbarOptionsMenu : function(type)
		{
			var parent = null;
			// Options menu
			var menupopup = null;

			// Options menu label on the intercept popup
			var entry = null;
			
			// Options menu on the intercept popup showing third party domains
			if (type == "toolbar")
			{

				parent = document.getElementById("priv3-intercept-menu");
				entry = document.getElementById("priv3-toolbar-options-popup")
				// XXX: first time menu item is created
				if (entry == null)
				{
					entry = document.createElement("menu");
					entry.setAttribute("id", "priv3-toolbar-options-popup");
					entry.setAttribute("label", "Priv3+ options");
					entry.setAttribute("popup", "priv3-toolbar-menu");

					menupopup = document.createElement("menupopup");
					menupopup.setAttribute("id", "priv3-toolbar-menu");
					menupopup.setAttribute("sizetopopup", "none");
					entry.appendChild(menupopup);
					parent.appendChild(entry);
				
					var sep = document.createElement("menuseparator");
					sep.setAttribute("id", "priv3-" + type + "-options_separator");
					parent.appendChild(sep);
					var sep = document.createElement("menuseparator");
					sep.setAttribute("id", "priv3-" + type + "-options_separator");
					parent.appendChild(sep);
				}
				else
				{
					menupopup = document.getElementById("priv3-toolbar-menu");
				}

			}
			else if (type == "tools")
			{
				menupopup = document.getElementById("priv3-tools-menu");
			}

			this.populateOptionsPopup(menupopup, type);
			
		},
		createStatsMenu : function(filteredItems, exceptedItems, reloadedItems, site)
		{
			var parent = document.getElementById("priv3-intercept-menu");
			var el = parent.firstChild;
			while(el != null) {
				var next = el.nextSibling;
				if(el.getAttribute("id").slice(0,12) == "priv3-status") {
					parent.removeChild(el);
				}
				el = next;
			}
			
			var entry = null;

			var flag1 = true;
			var flag2 = true;
			var flag3 = true;
			for (var i in filteredItems)
			{
				if (flag1)
				{
					flag1 = false;
					entry = document.createElement("menuitem");
					entry.setAttribute("label", "Filtered:");
					entry.setAttribute("id", "priv3-status_title");
					parent.appendChild(entry);					
				}
				var plugin = filteredItems[i];
				entry = document.createElement("menuitem");
				entry.setAttribute("id", "priv3-status_" + i);

				// XXX: not excepted, not reloaded
				if (plugin.length)
				{
					entry.setAttribute("label", i + " : " + plugin.length);
					// XXX: for easier exception addition
					var plu = {};
					plu["thirdparty"] = i;
					plu["site"] = site;
					entry.setAttribute("sitedata", JSON.stringify(plu));
					entry.addEventListener("click", function(event) { return function() {Priv3.Priv.onMenuItemCommand(event, 'exceptionlist', this.getAttribute("sitedata"));}}(entry), false);

					parent.appendChild(entry);					
				}
			}

			for (var i in exceptedItems)
			{
				if (flag2)
				{
					flag2 = false;
					var sep = document.createElement("menuseparator");
					sep.setAttribute("id", "priv3-status_separator");
					parent.appendChild(sep);
					var sep = document.createElement("menuseparator");
					sep.setAttribute("id", "priv3-status_separator");
					parent.appendChild(sep);
								
					entry = document.createElement("menuitem");
					entry.setAttribute("label", "Excepted:");
					entry.setAttribute("id", "priv3-status_title");
					parent.appendChild(entry);

				}
				var plugin = exceptedItems[i];
				entry = document.createElement("menuitem");
				entry.setAttribute("id", "priv3-status_" + i);

				entry.setAttribute("label", i + " : " + "excepted on: " + site);
				entry.addEventListener("click", function(event) {Priv3.Priv.onMenuItemCommand(event, 'exceptionlist', '');}, false);

				parent.appendChild(entry);					
			}

			for (var i in reloadedItems)
			{
				if (flag3)
				{
					flag3 = false;
					var sep = document.createElement("menuseparator");
					sep.setAttribute("id", "priv3-status_separator");
					parent.appendChild(sep);
					var sep = document.createElement("menuseparator");
					sep.setAttribute("id", "priv3-status_separator");
					parent.appendChild(sep);
		
					entry = document.createElement("menuitem");
					entry.setAttribute("label", "Reloaded:");
					entry.setAttribute("id", "priv3-status_title");
					parent.appendChild(entry);					
				}
				var plugin = reloadedItems[i];
				entry = document.createElement("menuitem");
				var text = "";
				
				for (var t in plugin)
				{
					text += t + ":" + plugin[t].length + ", ";
				}
				text = text.substring(0, text.length-2);
				
				entry.setAttribute("label", i + "(" + text + ")");
				// XXX: for easier exception addition
				var plu = {};
				plu["thirdparty"] = i;
				plu["site"] = site;
				entry.setAttribute("sitedata", JSON.stringify(plu));
				entry.addEventListener("click", function(event) { return function() {Priv3.Priv.onMenuItemCommand(event, 'exceptionlist', this.getAttribute("sitedata"));}}(entry), false);

				entry.setAttribute("id", "priv3-status_" + i);
				parent.appendChild(entry);
			}
			
		},
	}

	var Privlistener = {
		observe: function(subject, topic, data) {
			switch (topic) {
				case 'sessionstore-windows-restored':
				window.gBrowser.selectedTab = window.gBrowser.addTab(Priv3.extensionHomepage);
				Priv.unregister();
				break;
			}
		}
	}

	var Priv = {

		installButton: function(toolbarId, id, afterId)
		{
			if (document.getElementById(id)) 
			{
				return;
			}
			
			var toolbar = document.getElementById(toolbarId);
	
			// If no afterId is given, then append the item to the toolbar
			var before = null;
			if (afterId)
			{
				let elem = document.getElementById(afterId);
				if (elem && elem.parentNode == toolbar)
				{
					before = elem.nextElementSibling;
				}
			}
	
			toolbar.insertItem(id, before);
			toolbar.setAttribute("currentset", toolbar.currentSet);
			document.persist(toolbar.id, "currentset");
	
			if (toolbarId == "addon-bar")
			{
				toolbar.collapsed = false;
			}
		},
		
		onLoad: function() {
			var ver = parseFloat(prefHelpers.getPref("version", "char"));
			if(ver < parseFloat(Priv3.extensionVersion)) {
				this.register();
				prefHelpers.setPref("version", "char", Priv3.extensionVersion);
			}
			
			var firstRun = prefHelpers.getPref("firstRun", "boolean");
			if (firstRun)
			{
				this.installButton("nav-bar", "priv3-toolbar-item");
				prefHelpers.setPref("firstRun", "boolean", false);
			}
			
			loadHelpers.loadCurrentStatus();
		},
	
		status: function(){
			return statusHelpers.getCurrentStatus();
		},
	
		register: function() {
			var observerService = Components.classes["@mozilla.org/observer-service;1"].getService(Components.interfaces.nsIObserverService);
			observerService.addObserver(Privlistener, "sessionstore-windows-restored", false);
		},
		
		unregister: function(){
			var observerService = Components.classes["@mozilla.org/observer-service;1"].getService(Components.interfaces.nsIObserverService);
			observerService.removeObserver(Privlistener, "sessionstore-windows-restored");
		},

		onMenuItemCommand: function(e, type, info) {
			var item = null;
			switch(type) {
				case 'disable':
					statusHelpers.toggleCurrentStatus();
					break;
				
				case 'reportissue':
					var loc = gBrowser.contentDocument.location + "";
					var xulRuntime = Components.classes["@mozilla.org/xre/app-info;1"].getService(Components.interfaces.nsIXULRuntime);
					var appInfo = Components.classes["@mozilla.org/xre/app-info;1"].getService(Components.interfaces.nsIXULAppInfo);
					var platform = appInfo.vendor + "_" + appInfo.name + "_"+ appInfo.version + "_" + xulRuntime.OS;
					var date = new Date();
					var issue = "build=" + Priv3.buildInfo;
					issue += "&platform=" + platform;
					issue += "&url=" + loc;
					if (loc.indexOf("about:") == 0)
					{
						window.alert("Priv3+: Please use the 'report issue' feature on a non-empty page.");
						break;
					}
					//console.log(issue);
					var url = Priv3.extensionHomepage + "report.html?" + issue;
					var wm = Components.classes["@mozilla.org/appshell/window-mediator;1"].getService(Components.interfaces.nsIWindowMediator);
					var recentWindow = wm.getMostRecentWindow("navigator:browser");
					if(recentWindow)
					{
						recentWindow.gBrowser.selectedTab = recentWindow.gBrowser.addTab(url);
					}
					break;
					
					
				case 'about':
					window.open("chrome://priv3plus/content/about.xul", "About", "chrome,centerscreen");
					break;
				
				case 'exceptionlist':
					var plu = null;
					if (info != "")
					{
						plu = JSON.parse(info);
					}
					window.openDialog("chrome://priv3plus/content/exceptions.xul", "Exception options", "chrome,centerscreen", plu);
					break;
					
				case 'highlight':
					statusHelpers.toggleHighlight();
					break;
					
				case 'highlightoptions':
					window.openDialog("chrome://priv3plus/content/highlights.xul", "Highlight options", "chrome,centerscreen", null);
					break;
				
				case 'disableonsite':
					var url = gBrowser.contentDocument.location + "";
					var b = Priv3.listener.getBrowserFromURL(url);
					statusHelpers.toggleSiteStatus(b.site);
					break;
				
				case 'disableonpage':
					var url = gBrowser.contentDocument.location + "";
					statusHelpers.togglePageStatus(url);
					break;
				
				default:
					break;
			}
		},
		
		createToolbarMenu: function()
		{
			var url = gBrowser.contentDocument.location + "";
			var b = Priv3.listener.getBrowserFromURL(url);
			var items = menuHelpers.populateStatsMenu(b.blockedItems, b.exceptedItems, b.reloadedItems);
			menuHelpers.createToolbarOptionsMenu("toolbar");
			guiHelpers.setStatusCheckboxes("toolbar-", b.site, url);
			menuHelpers.createStatsMenu(items[0], items[1], items[2], b.site);
		},
		
		createToolsMenu: function()
		{
			var url = gBrowser.contentDocument.location + "";
			var b = Priv3.listener.getBrowserFromURL(url);
			var menupopup = document.getElementById("priv3-tools-menu");
			menuHelpers.populateOptionsPopup(menupopup, "tools");
			guiHelpers.setStatusCheckboxes("tools-", b.site, url);
		},

		onIconClick: function(e) {
			// toggle status on middle button click
			if (e.button == 1) {
				statusHelpers.toggleCurrentStatus();
			}
			
		}
	};

	function positionBadge(toolbarbadge)
	{
		var xulRuntime = Components.classes["@mozilla.org/xre/app-info;1"].getService(Components.interfaces.nsIXULRuntime);
		var right = -3;
		var bottom = -3;
		
		switch(xulRuntime.OS)
		{
			case "Darwin":
				right = -3;
				bottom = -3;
				break;
			case "Linux":
				right = -2;
				bottom = 0;
				break;
			case "WINNT":
				right = -1;
				bottom = 3;
				break;
			default:
				right = -3;
				bottom = -3;
		}

		toolbarbadge.setAttribute("right", right);
		toolbarbadge.setAttribute("bottom", right);

	};

	function setInterceptStatus() 
	{
		if(!gBrowser || !gBrowser.contentDocument)
		{
			return;
		}
		var url = gBrowser.contentDocument.location;
		if(!Priv3.listener || !Priv3.listener.getBrowserFromURL)
		{
			return;
		}
		
		var b = Priv3.listener.getBrowserFromURL(url);
		var blockedtext = "Priv3+ ";
		var itemtext = "";
		if (!Priv3.Priv.status())
		{
			blockedtext += "Disabled";				
		}
		else
		{
			if(!b || !b.blockedItems)
			{
				blockedtext += "Filtered: 0";
				itemtext += "Filtered: 0";
			}
			else
			{
				blockedtext += "Filtered: " + b.blockedItems.length;
				itemtext += "Filtered: " + b.blockedItems.length;
			}

			if (b.loadTime > 0)
			{
				blockedtext += ", load time: " + b.loadTime + " ms";
			}
		}

		var toolbarbutton = document.getElementById("priv3-toolbar-button");
		if(toolbarbutton)
		{
			toolbarbutton.tooltipText = blockedtext;
		}
		
		var toolbarbadge = document.getElementById("priv3-toolbar-badge");
		if (toolbarbadge)
		{
			var len = 0;
			if (b.blockedItems)
			{
				len = b.blockedItems.length;
			}
			toolbarbadge.setAttribute("value", len);
			positionBadge(toolbarbadge);
		}
		
	}
	
	var uninstallListener = {
		onUninstalling: function(addon) {
			prefHelpers.setPref("firstRun", "boolean", true);

			if (addon.id == Priv3.extensionId) {
				prefHelpers.setPref("version", "char", RESET_EXTENSION_VERSION);
			}
		}
	}

	try 
	{
		Components.utils.import("resource://gre/modules/AddonManager.jsm");
		AddonManager.addAddonListener(uninstallListener);

		Priv3.extensionId = "priv3plus@icsi.berkeley.edu";
		
		AddonManager.getAddonByID(Priv3.extensionId, function(addon) {
			Priv3.extensionVersion = addon.version;
			Priv3.extensionHomepage = addon.homepageURL;
        });
	} catch (ex) {}

	window.addEventListener("load", function(e) { Priv.onLoad(e); }, false);
	
	Priv3.Priv = Priv;
	Priv3.menuHelpers = menuHelpers;

	var setIntervalID = window.setInterval( function() { setInterceptStatus(); }, Priv3.refreshInterval);

	Priv3.setIntervalID = setIntervalID;
	Priv3.setInterceptStatus = setInterceptStatus;

	
})();
