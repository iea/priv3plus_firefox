/*
 * ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2011-2014 ICSI Center for Internet Research (ICIR)
 * All rights reserved.
 *
 * See LICENSE.txt for license and terms of usage. 
 *
 * ***** END LICENSE BLOCK *****
 */

Components.utils.import("resource://priv3plus/priv3helpers.jsm");

var Priv3HighlightOptions = {
	
	shouldHighlight: false,
	options: {},

	getHighlightOptions: function()
	{
		var box = document.getElementById("highlight-options-box");
		
		var el = box.firstChild;
		while (el != null)
		{
			var next = el.nextSibling;
			box.removeChild(el);
			el = next;
		}
		
		try
		{
			var item = document.createElement("checkbox");
			item.setAttribute("id", "highlight-checkbox");
			item.setAttribute("label", "Highlight third party content");
			item.addEventListener("command", function(event) {Priv3HighlightOptions.onCheckboxToggle(event, 'highlight');}, false);

			box.appendChild(item);
			
			this.shouldHighlight = prefHelpers.getPref("highlight", "boolean");
			if (this.shouldHighlight)
			{
				item.setAttribute("checked", "true");
			}
			
			var sep = document.createElement("menuseparator");
			box.appendChild(sep);
			
			
			this.options = JSON.parse(prefHelpers.getPref("highlightOptionsList", "char"));

			var labels = {};
			labels["main"] = "Highlight elements in main document (red)";
			labels["iframe"] = "Highlight elements in iframes (orange)";
			labels["other"] = "Highlight potential third party elements (yellow)";
			labels["clicked"] = "Highlight clicked elements (purple)";
			labels["reloaded"] = "Highlight reloaded (with cookies) elements (turquoise)";
			labels["excepted"] = "Highlight excepted elements (green)";
									
			for (var o in this.options)
			{
				var item = document.createElement("checkbox");
				item.setAttribute("id", "highlight-" + o + "-checkbox");
				item.setAttribute("label", labels[o]);
				
				if (this.options[o])
				{
					item.setAttribute("checked", "true");
				}
				
				item.addEventListener("command", function(event) { return function() {Priv3HighlightOptions.onCheckboxToggle(event, this.id);}}(item), false);
				
				if (!this.shouldHighlight)
				{
					item.setAttribute("disabled", "true");
				}
				
				box.appendChild(item);
			}
			
			var hbox = document.createElement("hbox");
			hbox.setAttribute("align", "right");

			var button = document.createElement("button");
			button.setAttribute("label", "Close");
			button.addEventListener("command", function(event) {window.close();}, false);
			
			hbox.appendChild(button);
			box.appendChild(hbox);
			
		}
		catch(e)
		{
			//console.log(e);
		}
	},
	
	onCheckboxToggle: function(event, id)
	{
		if (id == "highlight")
		{
			if (this.shouldHighlight)
			{
				this.shouldHighlight = false;
			}
			else
			{
				this.shouldHighlight = true;
			}
			
			prefHelpers.setPref("highlight", "boolean", this.shouldHighlight);
		}
		else
		{
			id = id.substring(10, id.indexOf("-checkbox"));
			if (this.options[id])
			{
				this.options[id] = false;
			}
			else
			{
				this.options[id] = true;
			}
			
			prefHelpers.setPref("highlightOptionsList", "char", JSON.stringify(this.options));
		}
		
		this.getHighlightOptions();
		
		highlightHelpers.toggleHighlightForAllBrowsers();

	}
}
