/*
 * ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2011-2014 ICSI Center for Internet Research (ICIR)
 * All rights reserved.
 *
 * See LICENSE.txt for license and terms of usage. 
 *
 * ***** END LICENSE BLOCK *****
 */

var Priv3ExceptionEditor = {
	params: {},
	result: null,
	init: function()
	{
		this.params = window.arguments[0];
		document.getElementById("editor-third-party-textbox").setAttribute("value", this.params["third-party"]);
		document.getElementById("editor-site-textbox").setAttribute("value", this.params["site"]);
	},
	
	save: function()
	{
		var thirdparty = document.getElementById("editor-third-party-textbox").value;
		var site = document.getElementById("editor-site-textbox").value;
		if (thirdparty == "" || site == "")
		{
			this.cancel();
			return;
		}
		
		if (this.params["third-party"] == thirdparty && this.params["site"] == site)
		{
			// XXX: nothing changed
			this.cancel();
			return;
		}
		
		this.result = {}
		this.result["third-party"] = thirdparty;
		this.result["site"] = site;
		
		window.close();
	},
	
	cancel: function()
	{
		window.close();
	},
	
};
