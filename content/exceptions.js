/*
 * ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2011-2014 ICSI Center for Internet Research (ICIR)
 * All rights reserved.
 *
 * See LICENSE.txt for license and terms of usage. 
 *
 * ***** END LICENSE BLOCK *****
 */

Components.utils.import("resource://priv3plus/priv3helpers.jsm");

var Priv3ExceptionsList = {
	_list: [],
	_treeView: {
		rowCount : 0,
		getCellText : function(row,column)
			{
				return Priv3ExceptionsList._list[row][column.id];
		  	},
		setCellText: function(row, column, val) {},
		setTree: function(treebox){ this.treebox = treebox; },
		isContainer: function(row){ return false; },
		isSeparator: function(row){ return false; },
		isSorted: function(){ return false; },
		getLevel: function(row){ return 0; },
		getImageSrc: function(row,col){ return null; },
		cycleHeader: function(aColId, aElt) {},
		isEditable: function(row, col){ return col.editable; },
		getCellValue : function(row,column)
		{
			return Priv3ExceptionsList._list[row][column.id];
		},
		setCellValue: function(row, col, val) {},
	},
	
	setExceptionsListView: function()
	{

		try
		{
			var ael = prefHelpers.getPref("allowedExceptionsList", "char");
			
			ael = JSON.parse(ael);
			
			this._list = [];

			for (var e in ael)
			{
				//console.log("third party: " + e);
				
				var sites = ael[e];
				for (var s in sites)
				{
					var arr = {};
					//console.log(sites[s]);
					arr["col-third-party"] = e;
					arr["col-type"] = "allowed on";
					arr["col-site"] = sites[s];
					this._list.push(arr);
				}				
			}
			
			this._treeView.rowCount = this._list.length;		
			
		}
		catch (e)
		{
			//console.log(e);
		}

		document.getElementById("exceptions-tree").view=this._treeView;
		
		var args = window.arguments[0];
		if (args != null)
		{
			var thirdparty = args["thirdparty"];
			var site = args["site"];
			document.getElementById("exceptions-textbox").setAttribute("value", thirdparty);
			document.getElementById("sites-textbox").setAttribute("value", site);
		}
    },

    addException : function()
	{
		var newdomain = document.getElementById("exceptions-textbox").value;
		var newsite = document.getElementById("sites-textbox").value;

		//console.log(newdomain + " " + newsite);

		if (newdomain == "" || newsite == "")
		{
			return;
		}
		
		document.getElementById("exceptions-textbox").value = "";
		document.getElementById("sites-textbox").value = "";
		
		this.addAllowedException(newdomain, newsite);
	
		this.setExceptionsListView();
	},

	addAllowedException : function(newdomain, newsite)
	{
	
		try
		{
			var el = prefHelpers.getPref("allowedExceptionsList", "char");
	
			el = JSON.parse(el);
			
			// XXX: initialization for new exceptions
			if (!el[newdomain])
			{
				el[newdomain] = [];
			}
	
			var sites = el[newdomain];
			
			// XXX: as if editing an existing exception that is already general with *
			if (sites[0] == "*")
			{
				return;
			}
			
			var newsites = newsite.split(",");
			for (var s in newsites)
			{
				var newsite = newsites[s];
				newsite = newsite.trim();
				if (newsite == "")
				{
					continue;
				}
				// XXX: shortcut for generalization
				else if (newsite == "*")
				{
					sites = [];
					sites[0] = newsite;
					break;
				}
				// XXX: else, add each newsite to the list
				if (sites.indexOf(newsite) == -1)
				{
					sites.push(newsite);
				}
			}
	
			el[newdomain] = sites;
			
			prefHelpers.setPref("allowedExceptionsList", "char", JSON.stringify(el));
		}
		catch (e)
		{
			//console.log(e);
		}
	
	},
    
    editException: function()
    {
		var rows = this._treeView.selection;
		var selected = rows.count;
		if (selected == 0)
		{
			return;
		}
		else if (selected > 1)
		{
			window.alert("Priv3+: Please select one element at a time to edit!");
			return;
		}
		
		var list = this._list;
		var selectedThird = null;
		var selectedSite = null;
		
		for (var i = 0; i < list.length; i++)
		{
			if (rows.isSelected(i))
			{
				selectedThird = list[i]["col-third-party"];
				selectedSite = list[i]["col-site"];
				break;
			}
		}
	
		var res = window.openDialog("editor.xul", "editor", "chrome,resizable=no,centerscreen,modal", {"third-party": selectedThird, "site": selectedSite});
	
		var newvalues = res.Priv3ExceptionEditor.result;
		
		if (newvalues == null)
		{
			return;
		}
		
		var newThird = newvalues["third-party"];
		var newSite = newvalues["site"];
		
		try
		{
			var el = prefHelpers.getPref("allowedExceptionsList", "char");
			el = JSON.parse(el);
			
			var sites = el[selectedThird];
			if (selectedThird == newThird)
			{
				for (var s in sites)
				{
					if (sites[s] == selectedSite)
					{
						sites[s] = newSite;
						break;
					}
				}
				el[selectedThird] = sites;
			}
			else
			{
				// XXX: first delete it from the old sites
				var newsites = [];
				for (var s in sites)
				{
					if (sites[s] != selectedSite)
					{
						newsites.push(sites[s]);
					}
				}
				if (newsites.length > 0)
				{
					el[selectedThird] = newsites;
				}
				else
				{
					delete el[selectedThird];
				}
				
				// XXX: then add it to the new third party sites
				var othersites = el[newThird];
				if (!othersites)
				{
					othersites = [];
				}
				if (newSite == "*")
				{
					othersites.push(newSite);
				}
				else if (othersites.indexOf(newSite) == -1)
				{
					othersites.push(newSite);
				}
				el[newThird] = othersites;
				
			}
			
			prefHelpers.setPref("allowedExceptionsList", "char", JSON.stringify(el));
		}
		catch (e)
		{
			//console.log(e);
		}
		
		this.setExceptionsListView();
	},

	deleteException: function()
	{
		var rows = this._treeView.selection;
		var selected = rows.count;
		if (selected == 0)
		{
			return;
		}
	
		var res = window.openDialog("confirmdelete.xul", "confirmdelete", "chrome,resizable=no,centerscreen,modal", {type: "delete", count: selected});
		if (res.Priv3ExceptionDeleter.result == 0)
		{
			return;
		}
		
		var list = this._list;
	
		try
		{
			var ael = prefHelpers.getPref("allowedExceptionsList", "char");
	
			ael = JSON.parse(ael);
	
			for (var i = 0; i < list.length; i++)
			{
				if (rows.isSelected(i))
				{
					var delDomain = list[i]["col-third-party"];
					var delSite = list[i]["col-site"];
					var delType = list[i]["col-type"];
					
					var sites = ael[delDomain];
					var newsites = [];
					for (var s in sites)
					{
						if (sites[s] != delSite)
						{
							newsites.push(sites[s]);
						}
					}
					switch(delType)
					{
						case "allowed on":
							if (newsites.length > 0)
							{
								ael[delDomain] = newsites;
							}
							else
							{
								delete ael[delDomain];
							}
							break;
						// TODO: advanced users disallowed exceptions
						default:
							break;
					}
				}
			}
			prefHelpers.setPref("allowedExceptionsList", "char", JSON.stringify(ael));
		}
		catch (e)
		{
			//console.log(e);
		}
		
		this.setExceptionsListView();
	
	},

    clearExceptionList: function()
    {
		var res = window.openDialog("confirmdelete.xul", "confirmdelete", "chrome,resizable=no,centerscreen,modal", {type: "clearall"});
		if (res.Priv3ExceptionDeleter.result == 0)
		{
			return;
		}
		
		var el = {};
		try
		{
			prefHelpers.setPref("allowedExceptionsList", "char", JSON.stringify(el));
		}
		catch (e)
		{
			//console.log(e);
		}
	
		this.setExceptionsListView();
	
	},
	
};

