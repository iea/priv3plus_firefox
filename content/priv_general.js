/*
 * ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2011-2014 ICSI Center for Internet Research (ICIR)
 * All rights reserved.
 *
 * See LICENSE.txt for license and terms of usage. 
 *
 * ***** END LICENSE BLOCK *****
 */

(function() {
	
	var extStatus = function()
	{
		try
		{
			return prefHelpers.getPref("status", "boolean");
		}
		catch (e)
		{
		}
		return false;
	};
	
	var reload = function(elems, plugin, browser)
	{
		if (!browser.reloadedItems[plugin])
		{
			browser.reloadedItems[plugin] = {};
		}
		
		var shouldHighlight = prefHelpers.getPref("highlight", "boolean");

		for(var i = 0; i < elems.length; i++)
		{
			var e = elems[i];
			switch(e.tagName) 
			{
				case "IFRAME":
					if(e && e.contentDocument)
					{
						if (e.src != "" || e.contentDocument.location != "about:blank")
						{
							// XXX: init reloaded iframe items
							if (!browser.reloadedItems[plugin]["iframe"])
							{
								browser.reloadedItems[plugin]["iframe"] = [];
							}
							if (browser.reloadedItems[plugin]["iframe"].indexOf(e.src) == -1)
							{
								// XXX: add it to the list
								// and do not reload even if the user clicks again.
								browser.reloadedItems[plugin]["iframe"].push(e.src);

								//console.log("priv3 reloading iframe: " + e.contentDocument.location);
								e.contentDocument.location.reload(true);
								// XXX: indicate we reloaded; change the highlight
								if (!e.getAttribute("priv3_clicked"))
								{
									var type = "RELOADED_ELEMENT";
									if (shouldHighlight)
									{
										highlightHelpers.highlight(e, type, browser);
									}
									
									browser.highlightList.push([e, type, highlightHelpers.getElementStyle(e)]);

									e.setAttribute("priv3_reloaded", "1");
								}
							}
							else
							{
								//console.log("priv3: already reloaded iframe: " + e.src);
							}
						}
					}
					break;
/*				case "SCRIPT":
					// XXX: init reloaded script items
					if (!browser.reloadedItems[plugin]["script"])
					{
						browser.reloadedItems[plugin]["script"] = [];
					}
					if (browser.reloadedItems[plugin]["script"].indexOf(e.src) == -1)
					{
						// XXX: add it to the list
						// and do not reload even if the user clicks again.
						browser.reloadedItems[plugin]["script"].push(e.src);

						var parent = e.parentNode;
						var script = e.ownerDocument.createElement("script");
						script.src = e.src;
						script.async = false;
						if(parent != null)
						{
							parent.insertBefore(script, e);
							parent.removeChild(e);
						}
						else
						{
							e.ownerDocument.body.appendChild(script);
						}
						//console.log("priv3 reloading script: " + e.src);
					}
					break;
*/
				default:
					break;
			}
		}
		
		if(Priv3.setIntervalID && Priv3.setInterceptStatus) {
			// clear the status bar
			window.clearInterval(Priv3.setIntervalID);
			Priv3.setIntervalID = null;

			// reset the status bar
			window.setTimeout(function(){
				Priv3.setIntervalID = window.setInterval( function() { Priv3.setInterceptStatus(); }, Priv3.refreshInterval);
			}, 5000);
		}
	};
	
	var interceptClick = function(event, browser)
	{
		if (event.button != 0 && event.button != 1)
		{
			return;
		}
		var url = null;
		var target = event.originalTarget;
		var doc = target.ownerDocument;
		var linkNode = clickHelpers.getNodeIfTargetIsHyperLink(target);
		if(linkNode && linkNode.href)
		{
			url = linkNode.href;
			//console.log("linknode; intercepting url: " + url);
		}
		else
		{
			url = doc.location.href;
			//console.log("some url; intercepting url: " + url);
		}

		// XXX: we are now using the third party host as the plugin name rather than pre-defined plugin names
		var plugin = urlHelpers.getDomain(urlHelpers.getHostname(url));
		if (!plugin || (!browser.social[plugin] && !exceptionHelpers.isAllowedException(plugin, browser)))
		{
			plugin = urlHelpers.getDomain(urlHelpers.getHostname(doc.location.href));
		}
		
		//console.log("plugin: " + plugin);
		var shouldHighlight = prefHelpers.getPref("highlight", "boolean");

		if(plugin)
		{
			var iframes = domHelpers.getElementsByMultipleTagNames(browser.contentDocument, ["iframe"]);
			var iframe = null;

			// XXX: search the clicked element in an iframe in the main document
			var elementInMainIframe = false;
			for(var i = 0; i < iframes.length; i++) 
			{
				if(iframes[i].contentDocument == doc)
				{
					//console.log("found iframe in main while intercepting");
					elementInMainIframe = true;
					iframe = iframes[i];
					break;
				}
			}

			// XXX: if iframe is still null, that means the clicked element may be in a child iframe
			// look for the child iframe that may contain the clicked element
			if (iframe == null)
			{
				elementInMainIframe = false;
				var childIframes = domHelpers.getChildIframes(iframes);
				for(var i = 0; i < childIframes.length; i++) 
				{
					if(childIframes[i].contentDocument == doc)
					{
						//console.log("found child iframe while intercepting");
						elementInMainIframe = false;
						iframe = childIframes[i];
						break;
					}
				}
				
			}
			
			
			var type = "CLICKED_ELEMENT";

			if (iframe != null && !iframe.getAttribute("priv3_clicked"))
			{
				//console.log("reloading iframe for plugin: " + plugin);
				if (browser.social[plugin])
				{
					if (shouldHighlight)
					{
						highlightHelpers.highlight(iframe, type, browser);
					}
					
					browser.highlightList.push([iframe, type, highlightHelpers.getElementStyle(iframe)]);

					iframe.setAttribute("priv3_clicked", "1");

					if (!elementInMainIframe)
					{
						// XXX: this is a heuristic to determine whether the click was to an advertisement or 
						// a social media widget
						// advertisements can be loaded within multiple nested iframes; hence, the check
						// above within the child iframes
						// if so, it's probably an advertisement; therefore, no need to reload the parent iframe
						//console.log("not reloading the iframe");
						
						// XXX: don't send the cookies when a click to the advertisement happens
						// XXX: so no need to set the userInteraction to true.
					}
					else
					{
						// XXX: this element is probably a social media widget (and found in an iframe in the main
						// document). For preserving social networking functionality after the user interaction,
						// we need to reload the iframes belonging to this third party
						// XXX: this is an element in an iframe in the main document or an iframe
						// area the user clicked; reload 
						//console.log("reloading the iframe");

						// XXX: we just need to set the user interaction to true, so that it can load or reload some
						// iframes or new windows without stripping the cookies
						browser.social[plugin].userInteraction = true;

						reload(browser.social[plugin].savedElems, plugin, browser);
					}
				}
				else if (exceptionHelpers.isAllowedException(plugin, browser))
				{
					// XXX: it was already loaded with cookies!
					// no need to reload anything else.
					//highlightHelpers.highlight(iframe, "CLICKED_ELEMENT", browser);
					//iframe.setAttribute("priv3_clicked", "1");
				}
				else
				{
					//console.log("nothing to reload because plugin had no cookies (i.e., social[plugin] == null)");
				}
			}
			else
			{
				// XXX: the clicked element was not in an iframe
				// XXX: must be in the main
				//console.log("no iframe for plugin: " + plugin);
//				highlightHelpers.highlight(linkNode, "CLICKED_ELEMENT");
//				reload(browser.social[plugin].savedElems, plugin, browser);
				var elems = domHelpers.getElementsByMultipleTagNames(linkNode, ["img"]);
				for (var i = 0; i < elems.length; i++)
				{
					if (browser.social[plugin])
					{
						if (shouldHighlight)
						{
							highlightHelpers.highlight(elems[i], type, browser);
						}
	
						browser.highlightList.push([elems[i], type, highlightHelpers.getElementStyle(elems[i])]);
					}
				}
			}
		}
	};


	
	var listener = {
		observe : function(aSubject, aTopic, aData) {
			// status is true when the extension is enabled
			if(!extStatus || !extStatus()) {
				return;
			}

			if(typeof Components == "undefined") {
				return;
			}

			var httpChannel = aSubject.QueryInterface(Components.interfaces.nsIHttpChannel);
			// get the browser that fired the http-on-modify request
			var browser = this.getBrowserFromChannel(aSubject);
			
			var host = httpChannel.getRequestHeader("Host");
			var requestURL = aSubject.URI.spec;
			cookieHelpers.getDefaultCookieAccess(aSubject);
			
			var plugin = null;
			if (browser != null)
			{
				if (!browser.inited)
				{
					initHelpers.initBrowser(browser);
					//console.log("previously not inited; now inited.");
				}
				
				var loc = browser.contentDocument.location + "";
				browser.loc = loc;
				
				var triggerURLHost = browser.contentDocument.location.hostname;
				
				if(triggerURLHost == "" && (aSubject.loadFlags & Components.interfaces.nsIChannel.LOAD_INITIAL_DOCUMENT_URI)) 
				{
					triggerURLHost = urlHelpers.getHostname(requestURL);
				}

				// XXX: Services.eTLD
				var triggerDomain = urlHelpers.getDomain(triggerURLHost);
				// XXX: useful for passing arguments to the exceptions window
				browser.site = triggerDomain;

				// XXX: check whole-site exceptions for preventing trackers (e.g., allow * on this site (e.g., "example.com"))
				if (exceptionHelpers.isDisabledSite(triggerDomain, browser))
				{
					return;
				}
				// XXX: check page exceptions for preventing trackers (e.g., allow * on this page (e.g., "example.com/page1.html"))
				if (exceptionHelpers.isDisabledPage(browser.loc, browser))
				{
					return;
				}

//				if (browser.postLoadFlag === false && loc.substring(0,6) != "about:")
				if (loc.substring(0,6) != "about:")
				{
					//console.log("setting timer: requestURL: " + requestURL + " " + loc);
					//browser.contentWindow.addEventListener("load", function(){ postLoadPage(browser); }, true);
					browser.postLoadTimeout = browser.contentWindow.setTimeout(function() {postLoadPage(browser);}, 1000);
					browser.postLoadFlag = true;
				}

	            // XXX: if there are no cookies with the request, there is no point in checking whether it's third party
	            var anyCookies = null;
				try
				{
					anyCookies = httpChannel.getRequestHeader("Cookie");
				}
				catch(err)
				{
					// XXX: no cookies associated with this request anyway
					// do nothing
					return;
				}
				
				// there is a cookie associated with this request; assume we are going to remove it
				var shouldRemoveCookies = true;
								
				// check whether the matching cookie covers the request's domain
				// if yes, send the cookie (i.e., shouldRemoveCookies = false)
				var requestURLHost = urlHelpers.getHostname(requestURL);

				var requestDomain = urlHelpers.getDomain(requestURLHost);
				
				if (requestDomain == triggerDomain)
				{
					shouldRemoveCookies = false;
				}
				
				// XXX: check for excepted trackers on specific sites
				// TODO: check for excepted tracker on specific pages??
				if (exceptionHelpers.checkAllowedExceptions(requestDomain, triggerDomain, browser))
				{
					shouldRemoveCookies = false;
					browser.exceptedItems.push([requestDomain, requestURL]);
				}
				
				// XXX: we determined that the request is to a third party, and there are cookies with the third party.
				// initialize the variables, such that the cookies can be removed and resent if there is user interaction
				// with the third party content.
				if (shouldRemoveCookies)
				{
					// 1. strip the cookie from the current request
					// when the page fully loads, we'll check the elements and highlight them according to their domain
					// 2. highlight the content to indicate it was loaded without sending cookies
					// 3. if the user interacts, make the request again with the original cookie value
					// 4. change the highlight to indicate that the user interacted with the content retrieved with the cookie

					plugin = requestDomain;
					
					//console.log(triggerDomain + " --> " + plugin + " remove: " + shouldRemoveCookies);
					
					browser.disallowedHosts.push(requestURLHost.slice(0, requestURLHost.lastIndexOf(".")));
					
					if (!browser.social[plugin])
					{
						browser.social[plugin] = {};
						browser.social[plugin].userInteraction = false;
						browser.social[plugin].savedElems = [];
					}
					
				}
			}
			
			// XXX: if the request is NOT a third party (i.e., user typed in facebook.com)
			// plugin will be null
			if(plugin != null)
			{
				if(!browser.social[plugin].userInteraction && !urlHelpers.HostAllowed(host, browser.disallowedHosts))
				{
					if(aTopic == "http-on-modify-request")
					{
 						//console.log("removing cookies from third party: " + plugin);
						var doc = browser.contentDocument;
						if(browser.scriptFlag === false)
						{
							browser.addEventListener("click", function(evt){ interceptClick(evt, browser); }, true);
							doc.addEventListener("beforescriptexecute", function(evt){ cookieHelpers.denyCookies(evt, browser); }, true);
					  		doc.addEventListener("afterscriptexecute", function(evt){ cookieHelpers.allowCookies(evt, browser); }, true);
					  		// XXX: store and highlight third party content when we are done
				  			browser.scriptFlag = true;
				  		}
						httpChannel.setRequestHeader("Cookie", null, false);
						browser.blockedItems.push([plugin, requestURL]);
						//browser.requestCookieList.push([requestURL, anyCookies]);
					}
				}
				// XXX: user has interacted with the plugin
				else
				{
					//console.log("user interaction: " + plugin);
				}
			}
		},
		
		getRequestLoadContext: function(aChannel) {
			try {
				if(aChannel && aChannel.notificationCallbacks)
				{
					return aChannel.notificationCallbacks.getInterface(Components.interfaces.nsILoadContext);
				}
			} catch (e) {
			}
			try {
				if(aChannel && aChannel.loadGroup && aChannel.loadGroup.notificationCallbacks)
				{
					return aChannel.loadGroup.notificationCallbacks.getInterface(Components.interfaces.nsILoadContext);
				}
			} catch (e) {
			}
			return null;
		},
	
		getWindowForRequest: function(aChannel) {
			var loadContext = this.getRequestLoadContext(aChannel);
			try {
				if(loadContext)
				{
					return loadContext.associatedWindow;
				}
			} catch (e) {
			}
			return null;
		},
	
		getDocument: function (aChannel) {
			try {
				var notificationCallbacks = aChannel.notificationCallbacks ? aChannel.notificationCallbacks : aChannel.loadGroup.notificationCallbacks;
				if (!notificationCallbacks)
			  		return null;
				var domWin = notificationCallbacks.getInterface(Components.interfaces.nsIDOMWindow);
				return domWin.document;
			}
			catch (e) {
				return null;
			}
		},
	
		getBrowserFromChannel: function (aChannel) {
			try {
				var notificationCallbacks = aChannel.notificationCallbacks ? aChannel.notificationCallbacks : aChannel.loadGroup.notificationCallbacks;
				if (!notificationCallbacks){
					return null;
			  	}
				var domWin = notificationCallbacks.getInterface(Components.interfaces.nsIDOMWindow);
				return gBrowser.getBrowserForDocument(domWin.top.document);
			}
			catch (e) {
				if(this.isXHR(aChannel)){
					var win = this.getWindowForRequest(aChannel);
					if(win && win.top && gBrowser)
					{
						if (gBrowser.getBrowserForDocument)
						{
							return gBrowser.getBrowserForDocument(win.top.document);
						}
						else
						{
							return null;
						}
					}
				}
				return null;
			}
		},
	
		getBrowserFromURL: function(url) {
			var wm = Components.classes["@mozilla.org/appshell/window-mediator;1"].getService(Components.interfaces.nsIWindowMediator);
			var browserEnumerator = wm.getEnumerator("navigator:browser");

			while (browserEnumerator.hasMoreElements()) {
				var browserWin = browserEnumerator.getNext();
				var tabbrowser = browserWin.gBrowser;

				var numTabs = tabbrowser.browsers.length;
				for (var index = 0; index < numTabs; index++) {
					var currentBrowser = tabbrowser.getBrowserAtIndex(index);
					if (url == currentBrowser.currentURI.spec) 
					{
					    return currentBrowser;
					}
				}
			}
			return null;
		},
	
		onPageUnload: function (evt){
			var target = evt.originalTarget.defaultView;
			if(target != target.top)
			{
				return;
			}
			var b = this.getBrowserFromURL(target.location);
			initHelpers.initBrowser(b);
		},
	
		// thanks to Firebug
		isXHR: function(aChannel) {
			try {
				var notificationCallbacks = aChannel.notificationCallbacks ? aChannel.notificationCallbacks : aChannel.loadGroup.notificationCallbacks;
				if (!notificationCallbacks)
					return false;
				var xhrRequest = notificationCallbacks ? notificationCallbacks.getInterface(Components.interfaces.nsIXMLHttpRequest) : null;
				return (xhrRequest != null);
			}
			catch (exc) {
			}
			return false;
		},
	
		QueryInterface : function(aIID) {
			if (aIID.equals(Components.interfaces.nsISupports) ||
		    		aIID.equals(Components.interfaces.nsIObserver))
		  		return this;
			throw Components.results.NS_NOINTERFACE;
		}
	};

	var checkIframe = function(iframe, browser)
	{
		var childIframes = [];
		var elemsIframe = domHelpers.getElementsByMultipleTagNames(iframe.contentDocument, domHelpers.tagsWithIframe);
		for (var j = 0; j < elemsIframe.length; j++)
		{
			 var elem = elemsIframe[j];
			 if (elem.tagName == "IFRAME")
			 {
				 childIframes.push(elem);
			 }
			 var plugin = urlHelpers.getElemPlugin(elem);
			 if (plugin && browser.social[plugin])
			 {
				return true;
			 }

		}
		// XXX: if we are here, we couldn't find any elements to a third party
		// in this iframe. It may still be the case that the child iframes
		// may have had an element
		for (var k = 0; k < childIframes.length; k++)
		{
			return checkIframe(childIframes[k], browser);
		}
		
		// XXX: if we are here, none of the elements were from a third party
		// none of the child iframes didn't have any elements from a third party
		// either.
		return false;

	};

	var saveAndHighlightThirdPartyContent = function(browser)
	{
	
		if (browser.highlightTrialCount == 6)
		{
			return;
		}

		//console.log("running postload save elements and highlight: " + browser.loc + " " + browser.highlightTrialCount);
		browser.highlightTrialCount++;

		var win = browser.contentWindow;
		
		var doc = browser.contentDocument;

		// XXX: check whether an element in the main document has been loaded from a third party
		var emain = [];
		var iframes = domHelpers.getElementsByMultipleTagNames(doc, ["iframe"]);
		var others = domHelpers.getElementsByMultipleTagNames(doc, domHelpers.tags);
		emain = emain.concat(iframes);
		emain = emain.concat(others);
		for (var i = 0; i < emain.length; i++)
		{
			var plugin = urlHelpers.getElemPlugin(emain[i]);
			if (plugin && browser.social[plugin])
			{
				// save it and mark it for the user
				browser.social[plugin].savedElems.push(emain[i]);

				if (!emain[i].getAttribute("priv3_clicked")
					&& !emain[i].getAttribute("priv3_reloaded"))
				{
					var type = "LOADED_ELEMENT_MAIN";
					
					// XXX: used when toggling highlight on/off
					browser.highlightList.push([emain[i], type, highlightHelpers.getElementStyle(emain[i])]);
					emain[i].setAttribute("priv3_main_element", "1");
				}
			}
			else if (plugin && exceptionHelpers.isAllowedException(plugin, browser))
			{
				emain[i].setAttribute("priv3_allowed_exception", "1");
				
				var type = "ALLOWED_EXCEPTION";

				// XXX: used when toggling highlight on/off
				browser.highlightList.push([emain[i], type, highlightHelpers.getElementStyle(emain[i])]);

			}
		}

		// XXX: request must be from for or within an iframe
		//console.log("req not in main: " + requestURL);
		// XXX: we got iframes above; now get all possible nested iframes
		// and the actual parents, because they may be javascript-generated, other frames (i.e., with no source)

		 for (var i = 0; i < iframes.length; i++)
		 {
		 	 var iframe = iframes[i];
		 	 if (iframe.getAttribute("priv3_main_element") == "1"
		 	 	 || iframe.getAttribute("priv3_clicked") == "1"
			 	 || iframe.getAttribute("priv3_reloaded") == "1")
		 	 {
		 	 	 continue;
		 	 }
		 	 
		 	 var result = checkIframe(iframe, browser);
		 	 if (result == true)
		 	 {
		 	 	 // XXX: found our iframe that either itself or its children
		 	 	 // caused a request to a third party; save all
		 	 	 //console.log("found responsible iframe: " + iframe.id);
		 	 	 
		 	 	 var elems = domHelpers.getElementsByMultipleTagNames(iframe.contentDocument, domHelpers.tagsWithIframe);
		 	 	 for (var j = 0; j < elems.length; j++)
		 	 	 {
		 	 	 	 var elem = elems[j];
		 	 	 	 var plugin = urlHelpers.getElemPlugin(elem);
		 	 	 	 if (plugin && browser.social[plugin])
		 	 	 	 {
	 	 	 	 	 	 browser.social[plugin].savedElems.push(elem);

						 elem.setAttribute("priv3_iframe_element", "1");
						 //highlightHelpers.highlight(elem, "LOADED_IFRAME_ELEMENT", browser);
						 // XXX: highlight the iframe containing this request, only if:
						 // 1) the request had a cookie attached (and got stripped) (i.e., existence of browser.social[plugin])
						 // 2) the iframe didn't get highlighted before (i.e., priv3_iframe_element was not set yet)
						 if (!iframe.getAttribute("priv3_iframe_element"))
						 {
						 	 var type = "LOADED_IFRAME_ELEMENT";
						 	 
						 	 // XXX: used when toggling highlight on/off
						 	 browser.highlightList.push([iframe, type, highlightHelpers.getElementStyle(iframe)]);

							 iframe.setAttribute("priv3_iframe_element", "1");
							 //console.log("highlightable iframe because of an element: " + elem.src + " " + plugin + " " + iframe.id);
						 }
		 	 	 	 }
		 	 	 	 else if (plugin && exceptionHelpers.isAllowedException(plugin, browser))
		 	 	 	 {
		 	 	 	 	 elem.setAttribute("priv3_allowed_exception", "1");
		 	 	 	 	 
		 	 	 	 	 var type = "ALLOWED_EXCEPTION";
		 	 	 	 	 
 	 					// XXX: used when toggling highlight on/off
 	 					browser.highlightList.push([elem, type, highlightHelpers.getElementStyle(elem)]);
		 	 	 	 }
		 	 	 }
		 	 }
		}
		
		// XXX: just highlight other iframes that may potentially have third party content
		// (e.g., javascript from a third party may have generated an iframe (twitter timeline))
		// the cleanest way to do this is to have a taint-propagation from script to generated content,
		// but we are not doing fancy things regarding taint-propagation, so this should be ok for
		// highlighting potential third party content; the user can decide to interact or not, and then 
		// we'll identify third party content (and reload relevant stuff)--which is our focus anyway
		// XXX: make sure that the iframe is actually not from first party
		for (var i = 0; i < iframes.length; i++)
		{
			var iframe = iframes[i];
			// XXX: don't overwrite what we have highlighted above
			if (iframe.getAttribute("priv3_main_element") == "1"
				|| iframe.getAttribute("priv3_iframe_element") == "1"
				|| iframe.getAttribute("priv3_allowed_exception") == "1"
				|| iframe.getAttribute("priv3_clicked") == "1"
				|| iframe.getAttribute("priv3_reloaded") == "1")
			{
				continue;
			}
			var plugin = urlHelpers.getElemPlugin(iframe);
			//console.log("other iframe: " + plugin + " " + browser.site)
			if (plugin != browser.site)
			{
				var type = "LOADED_IFRAME_OTHER";

				// XXX: used when toggling highlight on/off
				browser.highlightList.push([iframe, type, highlightHelpers.getElementStyle(iframe)]);

				iframe.setAttribute("priv3_other_iframe", "1");
			}
		}
		
		var others = domHelpers.getElementsByMultipleTagNames(doc, domHelpers.tagsVisibleExceptIframe);
		for (var i = 0; i < others.length; i++)
		{
			var other = others[i];
			// XXX: don't overwrite what we have highlighted above
			if (other.getAttribute("priv3_main_element") == "1"
				|| other.getAttribute("priv3_iframe_element") == "1"
				|| other.getAttribute("priv3_allowed_exception") == "1"
				|| other.getAttribute("priv3_clicked") == "1"
				|| other.getAttribute("priv3_reloaded") == "1")
			{
				continue;
			}
			var plugin = urlHelpers.getElemPlugin(other);
			//console.log("other element: " + plugin + " " + browser.site)
			if (plugin != browser.site)
			{
				var type = "LOADED_OTHER";

				// XXX: used when toggling highlight on/off
				browser.highlightList.push([other, type, highlightHelpers.getElementStyle(other)]);

				other.setAttribute("priv3_other", "1");
			}
		}		
		
		// XXX: saved elements; highlight them
		highlightHelpers.toggleElementHighlights(browser);
		
		var t = 1000 * browser.highlightTrialCount;
		browser.highlightTimeout = browser.contentWindow.setTimeout(function() {saveAndHighlightThirdPartyContent(browser);}, t);
	};

	var postLoadPage = function(browser)
	{
		if (browser.postLoadPageRunning)
		{
			return;
		}
		var win = browser.contentWindow;
		// XXX: most stable way to figure out whether a page has fully loaded, including iframes
		if (win && win.performance && win.performance.timing && win.performance.timing.loadEventEnd > 0)
		{
			//console.log("running post load");
			browser.postLoadPageRunning = true;
			// XXX: timer for page load
			var t = win.performance.timing.loadEventStart - win.performance.timing.navigationStart;
			//console.log("Time to load (" + browser.loc + "): " + t + " ms");
			browser.loadTime = t;

			// XXX: saving and highlighting content for reloading
			saveAndHighlightThirdPartyContent(browser);
			
		}
		else
		{
			//console.log("resetting post load timer");
			browser.postLoadTimeout = browser.contentWindow.setTimeout(function() {postLoadPage(browser);}, 5000);
		}
	};
	
	var initPriv = function(evt)
	{
		// init the first tab
		initHelpers.initBrowser(gBrowser.selectedBrowser);

		var observerService = Components.classes["@mozilla.org/observer-service;1"].getService(Components.interfaces.nsIObserverService);
	  	observerService.addObserver(listener, "http-on-modify-request", false);
	  	
	  	// init the subsequent tabs
//		gBrowser.tabContainer.addEventListener("TabOpen", initHelpers.initTab, false);
//		gBrowser.tabContainer.addEventListener("TabClose", initHelpers.initTab, false);		
	};
	
	
	window.addEventListener('load', initPriv, false);
	window.addEventListener('pagehide', function(event){
		if (event.originalTarget instanceof HTMLDocument) {
			if(listener && listener.onPageUnload)
			{
				listener.onPageUnload(event);
			}
		}
	}, false);
	

	
	Priv3.listener = listener;

})();
