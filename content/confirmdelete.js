/*
 * ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2011-2014 ICSI Center for Internet Research (ICIR)
 * All rights reserved.
 *
 * See LICENSE.txt for license and terms of usage. 
 *
 * ***** END LICENSE BLOCK *****
 */
 
var Priv3ExceptionDeleter = {
	
	result: null,
	init: function()
	{
		var type = window.arguments[0].type;
		var count = window.arguments[0].count;
		if (type == "delete")
		{
			if (count > 1)
			{
				document.getElementById("delete-confirm-desc").setAttribute("value", "Confirm deletion of " + count + " excepted third party domains?");
			}
			else
			{
				document.getElementById("delete-confirm-desc").setAttribute("value", "Confirm deletion of " + count + " excepted third party domain?");				
			}
		}
		else if (type == "clearall")
		{
			document.getElementById("delete-confirm-desc").setAttribute("value", "Confirm clearing of the entire exception list for third party domains?");			
		}
	},

	confirm: function()
	{
		this.result = 1;
		window.close();
	},
	
	cancel: function()
	{
		this.result = 0;
		window.close();
	},
	
};
