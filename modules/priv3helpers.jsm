/*
 * ***** BEGIN LICENSE BLOCK *****
 *
 * Copyright (c) 2011-2014 ICSI Center for Internet Research (ICIR)
 * All rights reserved.
 *
 * See LICENSE.txt for license and terms of usage. 
 *
 * ***** END LICENSE BLOCK *****
 */

var EXPORTED_SYMBOLS = ["urlHelpers", "exceptionHelpers", "clickHelpers", "cookieHelpers", 
						"domHelpers", "highlightHelpers", "prefHelpers", 
						"initHelpers"];

Components.utils.import("resource://gre/modules/Services.jsm");
Components.utils.import("resource://gre/modules/Timer.jsm");
Components.utils.import("resource://gre/modules/Console.jsm");
//Components.utils.import("resource://devtools/Console.jsm");

var urlHelpers = {
	getDomain : function(hostname)
	{
		try
		{
			return Services.eTLD.getBaseDomainFromHost(hostname);
		} 
		catch (e) 
		{
			
		}
		return hostname;
	},
	
	getElemURL : function(elem)
	{
		if (elem.tagName == "OBJECT" && elem.data)
		{
			return elem.data;
		}
		else if (elem.tagName == "LINK" && elem.href)
		{
			return elem.href;
		}
		else if (elem.src)
		{
			return elem.src;
		}
		return null;
	},
	
	getElemPlugin : function(elem)
	{
		var url = this.getElemURL(elem);
		if (url)
		{
			return urlHelpers.getDomain(urlHelpers.getHostname(url));
		}
		return null;
	},
	
	getHostname : function(url) {
		var re = new RegExp('^(?:f|ht)tp(?:s)?\://([^/]+)', 'im');
		var match = url.match(re);
		if(match)
		{
			return match[1].toString();
		}
		return null;
	},

	HostAllowed : function(host, disallowedHosts){
		for(var i in disallowedHosts) {
			var re = new RegExp(disallowedHosts[i] + "\.");
			if(host.match(re))
				return false;
		}
		return true;
	}

};

var exceptionHelpers = {
	isDisabledSite: function(site, browser)
	{
		var disabledSitesList = JSON.parse(prefHelpers.getPref("disabledSitesList", "char"));
		if (disabledSitesList.indexOf(site) == -1)
		{
			return false;
		}
		return true;
	},
	
	isDisabledPage: function(url, browser)
	{
		var disabledPagesList = JSON.parse(prefHelpers.getPref("disabledPagesList", "char"));
		var index = url.indexOf("#");
		if (index != -1)
		{
			url = url.substring(0, index);
		}

		if (disabledPagesList.indexOf(url) == -1)
		{
			return false;
		}
		return true;
	},
	
	addDisabledSite: function(site)
	{
		if (site == null)
		{
			return;
		}
		var disabledSitesList = JSON.parse(prefHelpers.getPref("disabledSitesList", "char"));
		if (disabledSitesList.indexOf(site) == -1)
		{
			disabledSitesList.push(site);
			prefHelpers.setPref("disabledSitesList", "char", JSON.stringify(disabledSitesList));
		}
	},
	
	addDisabledPage: function(url)
	{
		if (url.indexOf("about:") == 0)
		{
			return;
		}
		var disabledPagesList = JSON.parse(prefHelpers.getPref("disabledPagesList", "char"));
		var index = url.indexOf("#");
		if (index != -1)
		{
			url = url.substring(0, index);
		}

		if (disabledPagesList.indexOf(url) == -1)
		{
			disabledPagesList.push(url);
			prefHelpers.setPref("disabledPagesList", "char", JSON.stringify(disabledPagesList));
		}
	},
	
	removeDisabledSite: function(site)
	{
		var disabledSitesList = JSON.parse(prefHelpers.getPref("disabledSitesList", "char"));
		var index = disabledSitesList.indexOf(site);
		if (index != -1)
		{
			disabledSitesList.splice(index, 1);
			prefHelpers.setPref("disabledSitesList", "char", JSON.stringify(disabledSitesList));
		}
	},
	
	removeDisabledPage: function(url)
	{
		var disabledPagesList = JSON.parse(prefHelpers.getPref("disabledPagesList", "char"));
		var index = url.indexOf("#");
		if (index != -1)
		{
			url = url.substring(0, index);
		}

		var index2 = disabledPagesList.indexOf(url);
		if (index2 != -1)
		{
			disabledPagesList.splice(index2, 1);
			prefHelpers.setPref("disabledPagesList", "char", JSON.stringify(disabledPagesList));
		}
	},
	
	isAllowedException : function(thirdPartyDomain, browser)
	{
		if (browser.allowedExceptionHosts && browser.allowedExceptionHosts.indexOf(thirdPartyDomain) != -1)
		{
			return true;
		}
		return false;
	},
	
	checkAllowedExceptions : function(thirdPartyDomain, triggerDomain, browser)
	{
		if (!browser.allowedExceptionsList)
		{
			return false;
		}
		// XXX: shortcut for already-checked third party domains
		if (this.isAllowedException(thirdPartyDomain, browser))
		{
			//console.log("allowed: " + thirdPartyDomain + " on: " + triggerDomain);
			return true;
		}

		//console.log("checking third party: " + thirdPartyDomain + " on: " + triggerDomain);
		if (!browser.allowedExceptionsList[thirdPartyDomain])
		{
			// XXX: no entry for third party domain: not allowed exception
			return false;
		}
		var sites = browser.allowedExceptionsList[thirdPartyDomain];

		for (var s in sites)
		{
			var site = sites[s];
			//console.log("site: " + site);
			if (site == "*" || site == triggerDomain)
			{
				//console.log("allowed: " + thirdPartyDomain + " on: " + triggerDomain);
				browser.allowedExceptionHosts.push(thirdPartyDomain);
				return true;
			}
		}
		return false;
	},

};

var clickHelpers = {

	getNodeIfTargetIsHyperLink : function(node) 
	{
		var el = node;
		while ((el != null) && el.tagName != "BODY") 
		{
			if(el.tagName == "A")
			{
				return el;
			}
			else
			{
					//console.log("clicked el: " + el.tagName);
			}
			el = el.parentNode;
		}
		return null;
	},

};

var cookieHelpers = {

	defaultCookieAccess: [],

	getDefaultCookieAccess : function(aRequest) {
		var cookiePermissionService = Components.classes["@mozilla.org/cookie/permission;1"].getService(Components.interfaces.nsICookiePermission);
		aRequest.QueryInterface(Components.interfaces.nsIChannel);
		this.defaultCookieAccess[aRequest.URI.spec] = cookiePermissionService.canAccess(aRequest.URI, aRequest);
	},

	setCookieAccess : function(url, bool) {
		var permission = bool ? Components.interfaces.nsICookiePermission.ACCESS_ALLOW : Components.interfaces.nsICookiePermission.ACCESS_DENY;
		var uri = Components.classes["@mozilla.org/network/standard-url;1"].createInstance(Components.interfaces.nsIURI);
		uri.spec = url;
		var cookiePermissionService = Components.classes["@mozilla.org/cookie/permission;1"].getService(Components.interfaces.nsICookiePermission);
		if(bool) {
			permission = this.defaultCookieAccess[url];
		}
		cookiePermissionService.setAccess(uri, permission);
	},

	denyCookies : function(evt, browser) {
		var url = evt.target.src;
		var plugin = (url == null || url == "") ? null : urlHelpers.getDomain(urlHelpers.getHostname(url));
		if(plugin == null)
		{
			return;
		}
		var doc = browser.contentDocument;
		if(browser.social[plugin] && browser.social[plugin].userInteraction == false)
		{
			var ownerDoc = evt.target.ownerDocument;		
			if((ownerDoc.location == doc.location) && !urlHelpers.HostAllowed(url, browser.disallowedHosts)){
				this.setCookieAccess(doc.location.href, false);
			}
		}
	},

	allowCookies : function(evt, browser) {
		var url = evt.target.src;
		var plugin = (url == null || url == "") ? null : urlHelpers.getDomain(urlHelpers.getHostname(url));
		if(plugin == null)
		{
			return;
		}
		var doc = browser.contentDocument;
		if(browser.social[plugin] && browser.social[plugin].userInteraction == false)
		{
			var ownerDoc = evt.target.ownerDocument;
			if((ownerDoc.location == doc.location) && !urlHelpers.HostAllowed(url, browser.disallowedHosts)){
				this.setCookieAccess(doc.location.href, true);
				browser.social[plugin].savedElems.push(evt.target);
			}
		}
	}
};
	
var domHelpers = {

	tagsWithIframe : ["iframe", "img", "script", "link", "embed", "object"],
	tags : ["img", "script", "link", "embed", "object"],
	tagsVisibleExceptIframe : ["img", "embed", "object"],
	
	getElementsByMultipleTagNames : function(doc, tagnames)
	{
		var e = [];
		if (doc)
		{
			for (var t in tagnames)
			{
				var elems = doc.getElementsByTagName(tagnames[t]);
				for (var j = 0; j < elems.length; j++)
				{
					e.push(elems[j]);
				}
			}
		}
		return e;
	},
	
	getChildIframes : function(iframes)
	{
		var childIframes2 = [];
		for (var i = 0; i < iframes.length; i++)
		{
			var iframe = iframes[i];
			var iframeDoc = iframe.contentDocument || iframe.contentWindow.document;
			var childIframes = iframeDoc.getElementsByTagName("iframe");
			for (var j = 0; j < childIframes.length; j++)
			{
				childIframes2.push(childIframes[j]);
			}
		}

		if (childIframes2.length > 0)
		{
			return childIframes2.concat(this.getChildIframes(childIframes2));
		}
		
		return childIframes2;
	}

};
	
var highlightHelpers = {
	// if the element is an iframe, recursively retrieve all plugin names of it
	getAllPlugins : function(e, browser, tabid)
	{
		if (browser.highlightPluginList[tabid])
		{
			// XXX: cache of all the plugins for future use
			//console.log("using cached plugin info")
			return browser.highlightPluginList[tabid];
		}

		var plugins = [];
	  var pluginsOther = [];
	  var pluginsAllowedExceptions = [];
	  var iframes = [];
	  if (e.tagName == "IFRAME")
	  {
		  iframes.push(e);

		  iframes = iframes.concat(domHelpers.getChildIframes([e]));
		  for (var i = 0; i < iframes.length; i++)
		  {
			  var iframe = iframes[i];
			  var p = urlHelpers.getElemPlugin(iframe);
			  if (p != null && p != "" && p != browser.site)
			  {
				  //console.log("plugin iframe: " + p + " " + browser.social[p]);
				  if (exceptionHelpers.isAllowedException(p, browser))
				  {
					  if (pluginsAllowedExceptions.indexOf(p) == -1)
					  {
						  pluginsAllowedExceptions.push(p);
					  }
				  }
				  else if (browser.social[p])
				  {
					  if (plugins.indexOf(p) == -1)
					  {
						  plugins.push(p);
					  }
				  }
				  else if (!browser.social[p])
				  {
					  if (pluginsOther.indexOf(p) == -1)
					  {
						  pluginsOther.push(p);
					  }
				  }
			  }
			  var elems = domHelpers.getElementsByMultipleTagNames(iframe.contentDocument, domHelpers.tags);
			  //console.log("elems length: " + elems.length);
			  for (var j = 0; j < elems.length; j++)
			  {
				  var elem = elems[j];
				  var p = urlHelpers.getElemPlugin(elem);
				  //console.log("elem in iframe: " + elem.src);
				  if (p != null && p != "" && p != browser.site)
				  {
					  // console.log("plugin in an iframe: " + p + " " + browser.social[p]);
					  if (exceptionHelpers.isAllowedException(p, browser))
					  {
						  if (pluginsAllowedExceptions.indexOf(p) == -1)
						  {
							  pluginsAllowedExceptions.push(p);
						  }
					  }
					  else if (browser.social[p])
					  {
						  if (plugins.indexOf(p) == -1)
						  {
							  plugins.push(p);
						  }
					  }
					  else if (!browser.social[p])
					  {
						  if (pluginsOther.indexOf(p) == -1)
						  {
							  pluginsOther.push(p);
						  }
					  }
				  }
			  }
		  }
	  }
	  else
	  {
		  var p = urlHelpers.getElemPlugin(e);
		  //console.log(e.src + " " + p);
		  if (p != null && p != "" && p != browser.site)
		  {
			  //console.log("plugin element: " + p);
			  if (exceptionHelpers.isAllowedException(p, browser))
			  {
				  if (pluginsAllowedExceptions.indexOf(p) == -1)
				  {
					  pluginsAllowedExceptions.push(p);
				  }
			  }
			  else if (browser.social[p])
			  {
				  if (plugins.indexOf(p) == -1)
				  {
					  plugins.push(p);
				  }
			  }
			  else if (!browser.social[p])
			  {
				  if (pluginsOther.indexOf(p) == -1)
				  {
					  pluginsOther.push(p);
				  }
			  }
		  }
	  }
	  var pluginsAll = [plugins, pluginsOther, pluginsAllowedExceptions]; 
	  browser.highlightPluginList[tabid] = pluginsAll;
	  return pluginsAll;
  },
  
	getPluginsText : function(list)
	{
		var text = "";
		var tooltip = "";
		for (var i = 0; i < list.length; i++)
		{
			tooltip += list[i] + ", ";
			if (i < 3)
			{
				text += list[i] + ", ";
			}
		}
		
		if (list.length >= 4)
		{
			var rem = list.length - 3;
			text += "(" + rem + " more)";
		}
		else
		{
			text = text.substring(0, text.length-2);
		}

		tooltip = tooltip.substring(0, tooltip.length -2);
		
		return [text, tooltip];
	},
  
	getTextInfo : function(e, browser, tabid)
	{
		var plugins = this.getAllPlugins(e, browser, tabid);
		var allPlugins = plugins[0];
		var pluginsOther = plugins[1];
		var pluginsAllowedExceptions = plugins[2];
		// console.log(allPlugins);

		var text = "";
		var tooltip = "";

		if (allPlugins.length == 0 && pluginsOther.length == 0 && pluginsAllowedExceptions.length == 0)
		{
//			text = "(Potential third party content)";
//			tooltip = "Potential third party content (no cookies sent)";
		}
		else
		{
			if (allPlugins.length > 0)
			{
				text = "Third party content (cookies removed) (" + allPlugins.length + "): ";
				tooltip = "Third party content (cookies removed): ";

				var info = this.getPluginsText(allPlugins);
				text += info[0] + "&&&&&";
				tooltip += info[1] + "\n\n";
			}
			
			if (pluginsOther.length > 0)
			{
				text += "Third party content (no cookies) (" + pluginsOther.length + "): ";
				tooltip += "Third party content (no cookies): ";

				var info = this.getPluginsText(pluginsOther);
				text += info[0] + "&&&&&";
				tooltip += info[1] + "\n\n";
			}
			
			if (pluginsAllowedExceptions.length > 0)
			{
				text += "Third party content (allowed exceptions) (" + pluginsAllowedExceptions.length + "): ";
				tooltip += "Third party content (allowed exceptions): ";

				var info = this.getPluginsText(pluginsAllowedExceptions);
				text += info[0] + "&&&&&";
				tooltip += info[1] + "\n\n";
			}
			
			text = text.substring(0, text.length-5);
			tooltip = tooltip.substring(0, tooltip.length-2);
		}
		
		return [text, tooltip];

  },
  
	generateInfoTextId : function(e)
	{
		try
		{
			var tabid = "priv3_overlay_" + e.tagName + "_" + e.id + "_";
			if (e.src)
			{
				tabid += e.src;
			}
			else if (e.data)
			{
				tabid += e.data;
			}
			return tabid;
		}
		catch (e)
		{
		}
		return null;
  },
  
	drawTab : function(elem, event, browser)
	{
		var shouldHighlight = prefHelpers.getPref("highlight", "boolean");
		if (!shouldHighlight)
		{
			return;
		}

		var tabid = this.generateInfoTextId(elem);
		if (tabid == null)
		{
			return;
		}

		var doc = elem.ownerDocument.defaultView.top.document;
		var oldtab = doc.getElementById(tabid);

		if (oldtab)
		{
			// XXX: we already drew a tab for this object
			return;
		}
				
		var textContent = this.getTextInfo(elem, browser, tabid);

		var text = textContent[0];
		var tooltip = textContent[1];
		
		if (text == "" && tooltip == "")
		{
			// nothing to show in the box
			//console.log("nothing to show?");
			return;
		}

		//console.log(tabid);

		var tab = doc.createElement("div");

		tab.setAttribute("id", tabid);
		tab.setAttribute("align", "left");
		
		var tokens = text.split("&&&&&");
		for (var i = 0; i < tokens.length; i++)
		{
			var textnode = doc.createTextNode(tokens[i]);
			var br = doc.createElement("br");
			tab.appendChild(textnode);
			tab.appendChild(br);
		}

		tab.setAttribute("title", tooltip);

		tab.style.setProperty("position", "fixed", "important");
		tab.style.setProperty("display", "block", "important");
		tab.style.setProperty("background", "white", "important");
		tab.style.setProperty("border-style", "solid", "important");
		tab.style.setProperty("border-color", "red", "important");
		tab.style.setProperty("border-width", "3px", "important");
		tab.style.setProperty("line-height", "%100", "important");
		tab.style.setProperty("color", "black", "important");
		tab.style.setProperty("font-size", "10px", "important");
		tab.style.setProperty("font-family", "Arial,sans-serif", "important");
		tab.style.setProperty("z-index", "10000", "important");

		var rect = elem.getBoundingClientRect();
		tab.style.left = tab.style.setProperty("left", (event.clientX) + "px", "important");
		tab.style.top = tab.style.setProperty("top", (event.clientY) + "px", "important");

		tab.style.setProperty("opacity", "1", "important");
		
		//doc.documentElement.appendChild(tab);
		doc.body.appendChild(tab);

		setTimeout(function() { highlightHelpers.undrawTab(elem);}, 3000);
		event.preventDefault();
		event.stopPropagation();
		
	},
  
	undrawTab : function(elem)
	{
		var tabid = this.generateInfoTextId(elem);
		if (tabid == null)
		{
			return;
		}
		
		var doc = elem.ownerDocument.defaultView.top.document;
		var oldtab = doc.getElementById(tabid);
		if (oldtab)
		{
			//doc.documentElement.removeChild(oldtab);
			doc.body.removeChild(oldtab);
		}
	},
	
	delayDrawingTab : function(elem, browser)
	{
		var timeout = null;
		elem.addEventListener("mouseover", function(event)
			{
				timeout = setTimeout(function() { highlightHelpers.drawTab(elem, event, browser);}, 1000);
			}, false);
		
		elem.addEventListener("mouseout", function()
			{
				clearTimeout(timeout);
			}, false);
	},
	
	getElementStyle : function(elem)
	{
		let wnd = elem.ownerDocument.defaultView;
		var style = wnd.getComputedStyle(elem, null);
		return style;
	},
	
	// XXX: returns if element visible and its style
	isElementVisible : function(elem)
	{
		var style = this.getElementStyle(elem);
		if (style != null)
		{
			if (style.getPropertyValue("display") == "none")
			{
				return [false, style];
			}

			var width = style.getPropertyValue("width");
			var height = style.getPropertyValue("height");			
			if (width == "0px" || height == "0px" || width == "0" || height == "0")
			{
				return [false, style];
			}
		}
		return [true, style];
	},

	setOutline : function(e, type, browser)
	{
		var linetype = "3px dashed ";
		
		switch(type)
		{
			case "LOADED_ELEMENT_MAIN":
				// RED
				e.style.outline = linetype + "#ff0000";
				break;
			case "LOADED_IFRAME_ELEMENT":
				// ORANGE
				e.style.outline = linetype + "#ffa500";
				break;
			case "LOADED_IFRAME_OTHER":
			case "LOADED_OTHER":
				// YELLOW
				e.style.outline = linetype + "#fff000";
				break;
			case "CLICKED_ELEMENT":
				// PURPLE
				e.style.outline = linetype + "#f000ff";
				break;
			case "RELOADED_ELEMENT":
				// TURQUOISE
				e.style.outline = linetype + "#00f0ff";
				break;
			case "ALLOWED_EXCEPTION":
				// GREEN
				e.style.outline = linetype + "#00ff00";
				break;
			case "SELECTIVELY_RELOADED_ELEMENT":
				// BLUE
				e.style.outline = "solid 3px " + "#0000ff";
				break;
		}
		
        e.style.outlineOffset = "-3px";
	
	},
	
	unsetOutline : function(elem, style)
	{
		elem.style.outline = style.outline;
		elem.style.outlineOffset = style.getPropertyValue("outlineOffset");		
	},

	highlight : function(e, type, browser)
	{
		if (e.tagName == "SCRIPT")
		{
			return;
		}
		if (!e.getAttribute("priv3_listeners")
			&& type != "RELOADED_ELEMENT"
			&& type != "SELECTIVELY_RELOADED_ELEMENT")
		{
			this.delayDrawingTab(e, browser);
			e.setAttribute("priv3_listeners", "1");
		}

		this.setOutline(e, type, browser);

	},
	
	toggleElementHighlights : function(browser)
	{
		var shouldHighlight = prefHelpers.getPref("highlight", "boolean");

		var list = browser.highlightList;
		if (!list)
		{
			return;
		}
		
		for (var i = 0; i < list.length; i++)
		{
			var elem = list[i][0];
			var type = list[i][1];
			var style = list[i][2];
			
			var selectedHighlight = true;
			
			var highlightOptions = JSON.parse(prefHelpers.getPref("highlightOptionsList", "char"));
			
			switch(type)
			{
				case "LOADED_ELEMENT_MAIN":
					if (!highlightOptions["main"])
					{
						selectedHighlight = false;
					}
					break;
				case "LOADED_IFRAME_ELEMENT":
					if (!highlightOptions["iframe"])
					{
						selectedHighlight = false;
					}
					break;
				case "LOADED_IFRAME_OTHER":
				case "LOADED_OTHER":
					if (!highlightOptions["other"])
					{
						selectedHighlight = false;
					}
					break;
				case "CLICKED_ELEMENT":
					if (!highlightOptions["clicked"])
					{
						selectedHighlight = false;
					}
					break;
				case "RELOADED_ELEMENT":
					if (!highlightOptions["reloaded"])
					{
						selectedHighlight = false;
					}
					break;
				case "ALLOWED_EXCEPTION":
					if (!highlightOptions["excepted"])
					{
						selectedHighlight = false;
					}
					break;
			}

			if (shouldHighlight && selectedHighlight)
			{
				this.highlight(elem, type, browser);
			}
			else
			{
				this.unsetOutline(elem, style);
			}
		}
	},
	
	toggleHighlightForAllBrowsers : function()
	{
		var wm = Components.classes["@mozilla.org/appshell/window-mediator;1"].getService(Components.interfaces.nsIWindowMediator);
		var browserEnumerator = wm.getEnumerator("navigator:browser");

		while (browserEnumerator.hasMoreElements())
		{
			var browserWin = browserEnumerator.getNext();
			var tabbrowser = browserWin.gBrowser;

			var numTabs = tabbrowser.browsers.length;
			for (var index = 0; index < numTabs; index++)
			{
				var currentBrowser = tabbrowser.getBrowserAtIndex(index);
				//console.log("toggle highlight for browser: " + index);
				this.toggleElementHighlights(currentBrowser);
			}
		}
	},

};

var prefHelpers = {

	prefService: null,
	prefBranchName: "",

	initUserPrefs: function(b)
	{
		this.prefService = Components.classes["@mozilla.org/preferences-service;1"].getService(Components.interfaces.nsIPrefBranch);

		this.prefBranchName = "extensions.priv3plus.";
		
		// XXX: regular exceptions (i.e., "allow").
		b.allowedExceptionHosts = [];
		b.allowedExceptionsList = JSON.parse(this.getPref("allowedExceptionsList", "char"));
		
	},

	getPref : function(pname, type)
	{
		pname = this.prefBranchName + pname;

		if (this.prefService)
		{
			if (type == "boolean")
			{
				return this.prefService.getBoolPref(pname);
			}
			else if (type == "char")
			{
				return this.prefService.getCharPref(pname);
			}
		}
		else
		{
			if (type == "boolean")
			{
				return false;
			}
			else if (type == "char")
			{
				return "";
			}
		}
	},
	
	setPref : function(pname, type, val)
	{
		pname = this.prefBranchName + pname;
		
		if (this.prefService)
		{
			if (type == "boolean")
			{
				this.prefService.setBoolPref(pname, val);
			}
			else if (type == "char")
			{
				this.prefService.setCharPref(pname, val);
			}
		}
	},
	
};

var initHelpers = {

	initBrowser : function(b)
	{
		if(b == null || typeof b == "undefined")
		{
			return;
		}
		
		//console.log("initBrowser: " + b.postLoadFlag);

		b.scriptFlag = false;
		b.social = [];
		
		b.disallowedHosts = [];
		
		b.blockedItems = [];
		b.exceptedItems = [];
		b.reloadedItems = {};

		b.postLoadFlag = false;
		
		b.postLoadPageRunning = false;
		if (b.postLoadTimeout)
		{
			b.contentWindow.clearTimeout(b.postLoadTimeout);
		}
		b.postLoadTimeout = null;
		
		if (b.highlightTimeout)
		{
			b.contentWindow.clearTimeout(b.highlightTimeout);
		}
		b.highlightTimeout = null;
		b.highlightTrialCount = 0;
		b.highlightList = [];

		b.highlightPluginList = {};

		b.loadTime = 0;
		b.loc = null;
		b.site = null;

		prefHelpers.initUserPrefs(b);

		b.inited = true;

	},

/*	initTab : function(event)
	{
		var b = gBrowser.getBrowserForTab(event.target);
		this.initBrowser(b);
	}
*/

};

